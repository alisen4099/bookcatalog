
import { useState } from 'react';
import GenreFilter from '../components/GenreFilter'; 
import BookList from '../components/BookList';
import { css } from '@emotion/react';

const h1Style = css`
  font-size: 3.5rem;
  font-weight: bold;
  font-family: 'Times New Roman', serif;
  color: #333;
`;  


const containerStyle = css`
  font-size: 1.4rem;
  font-family: 'Times New Roman', serif;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;

  background-image: url('/images/3.jpg'); 
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  height: 97.9vh;
  width: 100%;

`;

const searchIconStyle = css`
  margin-top: 20px;
  cursor: pointer;
`;

const searchInputStyle = css`

  border-radius: 12px;
  border: 1px solid #ccc;
  padding: 10px;
  margin-left: 10px;

  font-size: 1.1rem;
  transition: border-color 0.3s ease;

  &:focus {
    border-color: #007bff;
    outline: none;
  }

  &::placeholder {
    color: #ccc;
  }
`;

type Book = {
  id: number;
  title: string;
  genre: string;
};

// Initial data
const books: Book[] = [
  { id: 1, title: '1984', genre: 'Classic' },
  { id: 2, title: 'Dune', genre: 'Science Fiction' },
  { id: 3, title: 'The Hobbit', genre: 'Fantasy' },
  { id: 4, title: 'The Catcher in the Rye', genre: 'Classic' },
  { id: 5, title: 'The Hunger Games', genre: 'Science Fiction' },
  { id: 6, title: 'The Da Vinci Code', genre: 'Mystery' },
  { id: 7, title: 'The Fellowship of the Ring', genre: 'Fantasy' },
  { id: 8, title: 'The Two Towers', genre: 'Fantasy' },
  { id: 9, title: 'The Return of the King', genre: 'Fantasy' },
  { id: 10, title: 'The Great Gatsby', genre: 'Classic' },
  { id: 11, title: 'The Hitchhiker\'s Guide to the Galaxy', genre: 'Science Fiction' },



];

const IndexPage = () => {
  const [selectedGenre, setSelectedGenre] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredBooks, setFilteredBooks] = useState(books);
  const [isSearchExpanded, setIsSearchExpanded] = useState(false);


  const handleGenreChange = (newGenre: string) => {
    setSelectedGenre(newGenre);
    filterBooks(newGenre, searchQuery);
  };

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSearchQuery = event.target.value;
    setSearchQuery(newSearchQuery);
    filterBooks(selectedGenre, newSearchQuery);
  };

  const toggleSearchExpand = () => {
    setIsSearchExpanded(!isSearchExpanded);
  };

  const filterBooks = (genre: string, query: string) => {
    let newFilteredBooks = books;

    if (genre) {
      newFilteredBooks = newFilteredBooks.filter((book) => book.genre === genre);
    }

    if (query) {
      newFilteredBooks = newFilteredBooks.filter((book) =>
        book.title.toLowerCase().includes(query.toLowerCase())
      );
    }

    setFilteredBooks(newFilteredBooks);
  };

  return (
    <div css={containerStyle}>
      <h1 css={h1Style}>Book Catalog</h1>

      <div>
        <span css={searchIconStyle} onClick={toggleSearchExpand}> Filter by Name: 🔍</span>
        {isSearchExpanded && (
          <input
            css={searchInputStyle}
            type="text"
            placeholder="Search by book name"
            value={searchQuery}
            onChange={handleSearchChange}
          />
        )}
      </div>
      <div>
        <span>Filter by Genre: </span>
        <GenreFilter selectedGenre={selectedGenre} onChange={handleGenreChange} />
      </div>
      <BookList books={filteredBooks} />
    </div>
  );
};


export default IndexPage;
