# BookCatalog

## Installing Required Packages
1. Make sure you have Node.js installed
2. Create a Next.js project with TypeScript support running the following commands:

```bash
npx create-next-app -e with-typescript book-catalog
```

```bash
 cd book-catalog
```

3. Install emotion.js and required packages
    
```bash
npm install @emotion/react @emotion/styled
```

## Running the App

1. Run the following command to start the app:

```bash
npm run dev
```