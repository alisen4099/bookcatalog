import React from 'react';
import { css, keyframes } from '@emotion/react';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;
const bookListStyle = css`
animation: ${fadeIn} 1s ease-in;

  list-style: none;
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 10px;
  padding-left: 10px;
  margin-top: 20px;
`;
const bookItemStyle = css`
  padding: 5px;
  font-size: 1.2rem;
  background-color: #f9f9f9;
  border: 1px solid #ccc;
  border-radius: 5px;
`;


type Book = {
  id: number;
  title: string;
  genre: string;
};

type BookListProps = {
  books: Book[];
};

const BookList: React.FC<BookListProps> = ({ books }) => {
  return (
    <ul css={bookListStyle}>
      {books.map((book) => (
        <li css={bookItemStyle} key={book.id}>
          {book.title} - {book.genre}
        </li>
      ))}
    </ul>
  );
};

export default BookList;
