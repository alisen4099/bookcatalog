import { useState } from 'react';
import { css } from '@emotion/react';

const dropdownStyle = css`
  padding: 10px;
  font-size: 1.1rem;
  border: 1px solid #ccc;
  border-radius: 5px;
  margin-top: 20px;
  text-indent: 0;
`;

const genres = ['Mystery', 'Science Fiction', 'Classic', 'Fantasy'];

type GenreFilterProps = {
  selectedGenre: string;
  onChange: (newGenre: string) => void;
};

const GenreFilter: React.FC<GenreFilterProps> = ({ selectedGenre, onChange }) => {
  const handleGenreChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const newGenre = event.target.value;
    onChange(newGenre);
  };

  return (
    <select css={dropdownStyle} value={selectedGenre} onChange={handleGenreChange}>
      <option value="">All Genres</option>
      {genres.map((genre, index) => (
        <option key={index} value={genre}>
          {genre}
        </option>
      ))}
    </select>
  );
};

export default GenreFilter;

